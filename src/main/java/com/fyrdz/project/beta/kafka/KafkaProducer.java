package com.fyrdz.project.beta.kafka;


import com.fyrdz.avro.schema.UpdateStock;
import com.fyrdz.project.beta.config.properties.KafkaCommonProperties;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Log4j2
@AllArgsConstructor
public class KafkaProducer {

    private KafkaTemplate<String, UpdateStock> kafkaTemplate;

    private final KafkaCommonProperties kafkaCommonProperties;

    public void send(UpdateStock product) {
        log.info("PUBLISHING TO KAFKA");
        ListenableFuture<SendResult<String, UpdateStock>> future = kafkaTemplate.send(kafkaCommonProperties.getTopicName(),
                product.getOrderId() ,product);
        future.addCallback(new ListenableFutureCallback<SendResult<String, UpdateStock>>() {
            @Override
            public void onFailure(Throwable ex) {
                log.error("ERROR when publish update stock cause: {}", ex.getMessage());
                log.error("Stack trace: {}", ExceptionUtils.getStackTrace(ex));
                log.info("END OF PUBLISHING TO KAFKA");
                throw new RuntimeException(ex);
            }

            @Override
            public void onSuccess(SendResult<String, UpdateStock> result) {
                log.info("SUCCESS publish update stock");
                log.info("END OF PUBLISHING");
            }
        });
    }
}
