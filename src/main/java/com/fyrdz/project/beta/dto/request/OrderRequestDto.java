package com.fyrdz.project.beta.dto.request;

import lombok.Data;

@Data
public class OrderRequestDto {
    private String id;
    private int quantity;
}
