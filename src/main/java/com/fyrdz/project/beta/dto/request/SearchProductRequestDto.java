package com.fyrdz.project.beta.dto.request;

import lombok.Data;

@Data
public class SearchProductRequestDto {
    private String name;
    private PagingValueRequestDto paging;
}
