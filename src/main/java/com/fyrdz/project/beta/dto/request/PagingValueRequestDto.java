package com.fyrdz.project.beta.dto.request;

import lombok.Data;

@Data
public class PagingValueRequestDto {
    private Integer previous;
    private Integer next;
    private Integer size;
}
