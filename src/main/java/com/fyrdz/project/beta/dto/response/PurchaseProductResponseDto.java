package com.fyrdz.project.beta.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PurchaseProductResponseDto {
    private String status;
    private String message;
}
