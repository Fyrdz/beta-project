package com.fyrdz.project.beta.dto.response;

import lombok.Data;

@Data
public class CursorDto {
    private Integer before;
    private Integer after;
}
