package com.fyrdz.project.beta.dto.response;

import lombok.Data;

@Data
public class PagingResponseDto {
    private CursorDto cursors;
    private Integer next;
}
