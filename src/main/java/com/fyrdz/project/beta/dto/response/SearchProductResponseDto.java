package com.fyrdz.project.beta.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class SearchProductResponseDto {
    private List<ProductResponseDto> products;
    private PagingResponseDto paging;
}
