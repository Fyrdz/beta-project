package com.fyrdz.project.beta.controller;

import com.fyrdz.project.beta.dto.request.OrderRequestDto;
import com.fyrdz.project.beta.dto.request.SearchProductRequestDto;
import com.fyrdz.project.beta.dto.response.PurchaseProductResponseDto;
import com.fyrdz.project.beta.dto.response.SearchProductResponseDto;
import com.fyrdz.project.beta.executor.PurchaseProductExecutor;
import com.fyrdz.project.beta.executor.SearchProductExecutor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/v1/market")
@Log4j2
public class ProductSellingController {

    @Autowired
    private SearchProductExecutor searchProductExecutor;

    @Autowired
    private PurchaseProductExecutor purchaseProductExecutor;

    @GetMapping("/search")
    public ResponseEntity<SearchProductResponseDto> searchActiveProduct(HttpServletRequest httpRequest,
                                                                        @RequestBody SearchProductRequestDto request) {
        return searchProductExecutor.searchProduct(httpRequest, request);
    }

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseProductResponseDto> purchaseProduct(HttpServletRequest httpRequest,
                                                                      @RequestBody List<OrderRequestDto> request) {
        return purchaseProductExecutor.purchaseProduct(httpRequest, request);
    }
}
