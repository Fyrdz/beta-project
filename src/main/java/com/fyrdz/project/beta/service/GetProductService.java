package com.fyrdz.project.beta.service;

import com.fyrdz.project.beta.dto.request.SearchProductRequestDto;
import com.fyrdz.project.beta.dto.response.LiveProductDetailsResponseDto;
import com.fyrdz.project.beta.dto.response.SearchProductResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "getProduct", url = "${fyrdz.alpha.apiUrl}")
public interface GetProductService {

    @PostMapping( "/search")
    SearchProductResponseDto getProducts(@RequestBody SearchProductRequestDto request);

    @PostMapping( "/search/{id}")
    LiveProductDetailsResponseDto getProductsDetail(@PathVariable String id);

}
