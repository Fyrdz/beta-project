package com.fyrdz.project.beta.config.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class KafkaCommonProperties {

    @Value("${props.kafka.producer.topic}")
    private String topicName;
}
