package com.fyrdz.project.beta.config;


import com.fyrdz.avro.schema.UpdateStock;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class KafkaProducerConfig {

    @Bean
    public ProducerFactory<String, UpdateStock> producerFactory(KafkaProperties kafkaProperties) {
        return new DefaultKafkaProducerFactory<>(kafkaProperties.buildProducerProperties());
    }

    @Bean
    public KafkaTemplate<String, UpdateStock> kafkaTemplate(ProducerFactory<String,
            UpdateStock> producerFactory) {
        return new KafkaTemplate<>(producerFactory);
    }
}
