package com.fyrdz.project.beta.executor;

import com.fyrdz.project.beta.dto.request.SearchProductRequestDto;
import com.fyrdz.project.beta.dto.response.SearchProductResponseDto;
import com.fyrdz.project.beta.service.GetProductService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Log4j2
@Component
public class SearchProductExecutor {

    @Autowired
    private GetProductService getProductService;

    public ResponseEntity<SearchProductResponseDto> searchProduct(HttpServletRequest httpRequest, SearchProductRequestDto request) {
        SearchProductResponseDto productList = getProductService.getProducts(request);
        return new ResponseEntity<>(productList, HttpStatus.OK);
    }
}
