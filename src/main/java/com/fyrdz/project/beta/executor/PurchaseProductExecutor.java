package com.fyrdz.project.beta.executor;

import com.fyrdz.avro.schema.Order;
import com.fyrdz.avro.schema.UpdateStock;
import com.fyrdz.project.beta.dto.request.OrderRequestDto;
import com.fyrdz.project.beta.dto.response.LiveProductDetailsResponseDto;
import com.fyrdz.project.beta.dto.response.PurchaseProductResponseDto;
import com.fyrdz.project.beta.kafka.KafkaProducer;
import com.fyrdz.project.beta.service.GetProductService;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@Component
public class PurchaseProductExecutor {

    @Autowired
    private GetProductService getProductService;

    @Autowired
    private KafkaProducer producer;

    public ResponseEntity<PurchaseProductResponseDto> purchaseProduct(HttpServletRequest httpRequest, List<OrderRequestDto> request) {
        List<Order> orderList = new ArrayList<>();
        for (OrderRequestDto order : request) {
            LiveProductDetailsResponseDto product = getProductService.getProductsDetail(order.getId());
            if (order.getQuantity() <= product.getQuantity()) {
                Order orderItem = Order.newBuilder()
                        .setId(order.getId())
                        .setQuantity(order.getQuantity())
                        .build();
                orderList.add(orderItem);
            }
        }
        UpdateStock updateStock = UpdateStock.newBuilder()
                .setOrderId(UUID.randomUUID().toString())
                .setOrderList(orderList).build();
        producer.send(updateStock);
        return new ResponseEntity<>(new PurchaseProductResponseDto("Success", "Success purchase product"), HttpStatus.OK);
    }
}
